﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace OpenCaption
{
    public class ScriptAlertHub : Hub
    {
        /// <summary>
        /// Notify user that their upload started
        /// </summary>
        /// <param name="file"></param>
        public void AlertStarted(string message)
        {
            Clients.Caller.started(message);
        }

        /// <summary>
        /// Notify user that their upload finished successfully
        /// </summary>
        /// <param name="file"></param>
        public void AlertSuccess(string message)
        {
            // For short scrips, let "started" animation finish
            System.Threading.Thread.Sleep(5000);
            var context = GlobalHost.ConnectionManager.GetHubContext<ScriptAlertHub>();
            context.Clients.All.success(message);
        }

        /// <summary>
        /// Notify user that their upload failed
        /// </summary>
        /// <param name="message"></param>
        public void AlertFail(string message)
        {
            // For short scrips or errors, let "started" animation finish
            System.Threading.Thread.Sleep(5000);
            var context = GlobalHost.ConnectionManager.GetHubContext<ScriptAlertHub>();
            context.Clients.All.error(message);            
        }

        /// <summary>
        /// Notify user that their export failed
        /// </summary>
        /// <param name="file"></param>
        /// <param name="message"></param>
        public void ExportFail(string file, string message)
        {
            // For short scrips or errors, let "started" animation finish
            System.Threading.Thread.Sleep(5000);
            Clients.Caller.errorFail("Export of script ID " + file + " failed with message: " + message);
        }

        /// <summary>
        /// Notify user that their export has started
        /// </summary>
        /// <param name="file"></param>
        public void ExportStarted(string file)
        {
            Clients.Caller.uploadSuccess("Export of script ID " + file + " started");
        }

        /// <summary>
        /// Notify user that their export has started
        /// </summary>
        /// <param name="file"></param>
        public void ExportSuccess(string file)
        {
            Clients.Caller.uploadSuccess("Export of script ID " + file + " completed. The file can be found at ....");
        }
    }
}