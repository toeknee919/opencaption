﻿$("#uploadForm").validate({
    rules: {
        scriptFile: {
            required: true,
            extension: "txt"
        }
    },
    messages: {
        scriptFile: {
            required: "Please select a .txt file to upload",
            extension: "We require a .txt file for this application"
        }
    },
    errorElement : 'div',
    errorLabelContainer: '.errorTxt'
});
