﻿using System.ComponentModel.DataAnnotations.Schema;

namespace OpenCaption.Models
{
    public class Operator
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public virtual Work Work { get; set; }

        public string Name { get; set; }

        public int Position { get; set; }

    }
}