﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Data.Entity;

namespace OpenCaption.Models
{
    public class ScriptManager
    {
        /// <summary>
        /// Generates a .txt version of a script that is loaded into the database.
        /// If you re-load this script using the uploader, it will look exactly 
        /// the same as when it was exported. This will not include operator notes.
        /// </summary>
        public static void GenerateTextScript(int scriptId, string filePath)
        {

            using (CaptionDataContext db = new CaptionDataContext())
            {
                // Get all the lines, in sequence, for that script
                List<Text> lines = db.Text.Where(t => t.Script.Id == scriptId).OrderBy(t => t.Sequence).ToList();
                Script script = db.Script.Where(s => s.Id == scriptId).Include(s => s.Work).First();
                string f = filePath + script.Work.Name + "_" + "_" + script.Language + "_" + DateTime.Now.ToString("MMddHHmm") + ".txt";

                // Write these lines to a text file
                using (StreamWriter file = new StreamWriter(f))
                {
                    foreach (var line in lines)
                    {
                        // Normal line
                        if (line.Element.Name != string.Empty)
                        {
                            file.WriteLine(line.Element.Name + ":" + line.ContentText);
                        }
                        // Other
                        else
                        {
                            if (line.Element.Type == "BLANKLINE")
                            {
                                file.WriteLine("##");
                            }
                            else if (line.Element.Type == "AUDIENCEMESSAGE")
                            {
                                file.WriteLine("##:" + line.ContentText);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Method is used for regenerating a script in the database to a different character count setting. If newCount is null, the script will
        /// be parsed to a max line length of Int32.MaxValue.
        /// </summary>
        public virtual void RegenerateScriptWithCharacterCount(Script script, int? newCount)
        {
            ScriptAlertHub alert = new ScriptAlertHub();
            
            if (script.HasTexts())
            {
                using (CaptionDataContext db = new CaptionDataContext())
                {
                    db.Configuration.AutoDetectChangesEnabled = false;

                    // Set the scripts new CharacterCount
                    script.CharacterCount = newCount ?? Int32.MaxValue;

                    // Update the created time
                    script.CreatedDate = DateTime.Now;
                    db.Entry(script).State = EntityState.Modified;

                    // Export Script to an array combining all monologues
                    List<Text> lines = db.Text.Where(t => t.Script.Id == script.Id).OrderBy(t => t.Sequence).ToList();
                    List<string> results = new List<string>();
                    StringBuilder sb = new StringBuilder();
                    string currentCharacter = String.Empty;

                    foreach (var line in lines)
                    {

                        // Dealing with character dialogue
                        if (line.Element.Name != string.Empty)
                        {
                            // Character Changed or starting a line after blankline/audiencemessage
                            if (currentCharacter != line.Element.Name || sb.Length == 0)
                            {
                                currentCharacter = line.Element.Name;
                                if (sb.Length > 0)
                                {
                                    results.Add(sb.ToString());
                                }
                                sb.Clear();
                                sb.Append(line.Element.Name).Append(":").Append(line.ContentText.Trim());
                            }
                            // Same Character
                            else
                            {
                                sb.Append(" ").Append(line.ContentText.Trim());
                            }
                        }
                        // Dealing with audience message or blank line (preserve formatting here)
                        else
                        {
                            // add any previous lines
                            if (sb.Length > 0)
                            {
                                results.Add(sb.ToString());
                                sb.Clear();
                            }
                            if (line.Element.Type == "BLANKLINE")
                            {
                                results.Add("##");
                            }
                            else if (line.Element.Type == "AUDIENCEMESSAGE")
                            {
                                results.Add("##:" + line.ContentText);
                            }
                        }
                    }

                    // Add any additional lines
                    results.Add(sb.ToString());

                    // Re-parse the script into the database
                    GenerateLines(script, results.ToArray(), db);
                }
            }
            else
            {
                throw new Exception("No lines in script");
            }
        }


        /// <summary>
        /// Gets a text file's encoding so parsing foreign texts is possible
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static Encoding GetScriptEncoding(string filePath)
        {
            return Util.EncodingDetector.GetFileEncoding(filePath);
        }

        /// <summary>
        /// Determines if a script contains any texts
        /// </summary>
        /// <param name="ScriptId"></param>
        /// <returns></returns>
        public static bool HasTexts(int ScriptId)
        {
            using (CaptionDataContext data = new CaptionDataContext())
            {
                Script item = data.Script.FirstOrDefault(s => s.Id == ScriptId);
                if (item != null)
                {
                    return item.CountTexts() > 0 ? true : false;
                }
                return false;
            }
        }

        /// <summary>
        /// Generates the lines and character elements into the database for a script.
        /// If the script already has lines in the databse, it will overwrite them with the
        /// new lines. Each line in the array should match the syntax expected for a .txt file.
        /// </summary>
        /// <param name="script">Script object that is populated with </param>
        /// <param name="lines">Array of lines to be parsed.</param>
        public void GenerateLines(Script script, String[] lines, CaptionDataContext data)
        {
            int i = 0;
            int textSequence = 1;
            Element currentCharacter = null;
            string elementName;
            string currentText = String.Empty;
            bool addedBlank = false;
            string[] textComponents;

            try
            {
                // remove the original texts for this script
                script.CleanTexts(data);

                // Get default elements
                Element BLANK = data.Element.First(e => e.Script.Id == script.Id && e.Type == "BLANKLINE");
                Element AUDIENCEMESSAGE = data.Element.First(e => e.Script.Id == script.Id && e.Type == "AUDIENCEMESSAGE");

                // Parse the script
                for (i = 0; i < lines.Length; i++)
                {

                    // Split the line if there is a character name
                    string[] line = lines[i].Split(new char[] { ':' }, 2);

                    // If line contains a Character name and text
                    if (line.Length == 2)
                    {
                        // As long as we know who is speaking
                        if (currentCharacter != null && addedBlank == false)
                        {
                            // Split the line down to the specified character count and add the lines to the database
                            textComponents = SegmentText(currentText.Trim(), currentCharacter.Name, script.CharacterCount);
                            foreach (var text in textComponents)
                            {
                                data.Text.Add(new Text()
                                {
                                    Sequence = textSequence,
                                    Element = currentCharacter,
                                    Script = script,
                                    Visible = true,
                                    ContentText = text

                                });
                                textSequence += 1;
                            }
                        }
                        // We might need to create the character
                        elementName = line[0].Trim();
                        currentText = line[1].Trim();
                        // Did the character change
                        if (currentCharacter == null || elementName != currentCharacter.Name)
                        {
                            // If this is a note to the audience
                            if (elementName == "##")
                            {
                                currentCharacter = AUDIENCEMESSAGE;
                            }
                            else
                            {
                                currentCharacter = data.Element.FirstOrDefault(e => e.Script.Id == script.Id && e.Name == elementName);
                            }

                        }

                        // If the character does not exist already for this script, create them.
                        if (currentCharacter == null)
                        {
                            currentCharacter = data.Element.Add(new Element()
                            {
                                Name = elementName,
                                Script = script,
                                Type = "CHARACTER"
                            });
                            // Save Element so it can be refrenced for a text
                            data.SaveChanges();
                        }

                        // Reset indicator if needed
                        addedBlank = false;
                    }

                    // If line only contains text
                    else if (line.Length == 1)
                    {
                        // If we recently added a blankline and need to pick up where we left off in a characters script
                        if (addedBlank && line[0].Trim() != "##")
                        {
                            currentText = line[0].Trim();
                            addedBlank = false;
                        }

                        // If we are adding additional blank lines to a previous
                        else if (addedBlank && line[0].Trim() == "##")
                        {
                            data.Text.Add(new Text()
                            {
                                Sequence = textSequence,
                                Element = BLANK,
                                Script = script,
                                Visible = true,
                                ContentText = "<br />"
                            });
                            textSequence += 1;
                        }

                        // If this is a blankline and the first time
                        else if (line[0].Trim() == "##")
                        {
                            // We need to add previous character's already built text to database before inserting the blankline
                            textComponents = SegmentText(currentText.Trim(), currentCharacter.Name, script.CharacterCount);
                            foreach (var text in textComponents)
                            {
                                data.Text.Add(new Text()
                                {
                                    Sequence = textSequence,
                                    Element = currentCharacter,
                                    Script = script,
                                    Visible = true,
                                    ContentText = text

                                });
                                textSequence += 1;
                            }

                            // Then add the blank line to the sequence
                            data.Text.Add(new Text()
                            {
                                Sequence = textSequence,
                                Element = BLANK,
                                Script = script,
                                Visible = true,
                                ContentText = "<br />"
                            });
                            textSequence += 1;
                            addedBlank = true;
                        }

                        // Keep building text for this character
                        else
                        {
                            currentText += " " + line[0].Trim();
                        }
                    }
                }

                // Finally add the last texts in the file
                textComponents = SegmentText(currentText.Trim(), currentCharacter.Name, script.CharacterCount);
                foreach (var text in textComponents)
                {
                    data.Text.Add(new Text()
                    {
                        Sequence = textSequence,
                        Element = currentCharacter,
                        Script = script,
                        Visible = true,
                        ContentText = text

                    });
                    textSequence += 1;
                }
            }

            // Notify the user what line caused a failure
            catch (Exception ex)
            {
                throw new Exception("Error At Line: " + i + " : " + ex.Message);
            }
            // Save all changes to the DB
            data.SaveChanges();

        }

        /// <summary>
        /// Segments a long line of text for a specified character down to no 
        /// longer than the character count set for the script. 
        /// </summary>
        /// <param name="fullLine">The text to be parsed</param>
        /// <param name="currentCharacter">Characters name</param>
        /// <returns></returns>
        public string[] SegmentText(string fullLine, string currentCharacter, int CharacterCount)
        {
            List<String> resultsArray = new List<string>();
            StringBuilder line = new StringBuilder();
            string[] words = fullLine.Split(' ');
            int limit = CharacterCount;

            // Adjust for character name length longer than the max CharacterCount set
            if ((currentCharacter + ":").Length > CharacterCount)
            {
                // Insert blank line to make space for name
                resultsArray.Add("");
            }
            else
            {
                limit -= (currentCharacter).Length + 2; // 2 for colon and space characters
            }

            for (int i = 0; i < words.Length; i++)
            {
                // Place next word into current string if total < limit
                if ((line.Length + words[i].Length) <= limit)
                {
                    line.Append(words[i]).Append(" ");
                }

                else
                {
                    // Add the current line to results array
                    resultsArray.Add(line.ToString());

                    // Start a new line
                    line.Clear().Append(words[i]).Append(" ");

                    // After first line, reset to full count for the rest of the lines
                    limit = CharacterCount;
                }
            }
            // Add remaining text before returning  
            resultsArray.Add(line.ToString());
            return resultsArray.ToArray();
        }
    }
}