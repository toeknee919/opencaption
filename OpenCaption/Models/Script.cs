﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Text;

namespace OpenCaption.Models
{
    public class Script : ScriptManager
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string FileName { get; set; }
        public int WorkId { get; set; }
        public Work Work { get; set; }
        public int VenueId { get; set; }
        public Venue Venue { get; set; }
        public virtual ICollection<Text> Texts { get; set; }
        public virtual ICollection<Element> Elements { get; set; }
        public int CharacterCount { get; set; }
        public bool Original { get; set; }
        [DisplayName("Language/Version")]
        public string Language { get; set; }
        public DateTime CreatedDate { get; set; }

        public void GenerateTextScript()
        {
            GenerateTextScript(Id, FileName);
        }


        /// <summary>
        /// Used for Parsing a new file into the database for the Uploads specified work
        /// </summary>
        /// <param name="file"></param>
        public void ParseNewScript()
        {
            // Get database context
            using (CaptionDataContext data = new CaptionDataContext())
            {
                // Speed up upload
                data.Configuration.AutoDetectChangesEnabled = false;

                // Get refrence to the work the script will be loaded for
                Work work = data.Work.First(w => w.Id == WorkId);
                this.Work = work;
                
                // Remove old texts and elements
                CleanTextsAndElements();

                CharacterCount = CharacterCount;
                CreatedDate = DateTime.Now;
                // Mark as updated to save character count
                data.Entry(this).State = System.Data.Entity.EntityState.Added;
                
                // Add elements for operator to use
                Element BLANK = data.Element.Add(new Element(){ Script = this, Name = String.Empty, Type = "BLANKLINE" }); // Represented as ##
                Element AUDIENCEMESSAGE = data.Element.Add(new Element(){ Script = this, Name = String.Empty, Type = "AUDIENCEMESSAGE" }); // ##: Message here
                data.SaveChanges();

                // Detects the encoding of the uploaded file
                Encoding encoding = Util.EncodingDetector.GetFileEncoding(FileName);

                // Read the file line by line into array
                string[] lines = File.ReadAllLines(FileName, encoding);

                GenerateLines(this, lines, data);
            }
        }

        


        /// <summary>
        /// Removes any items from the database that were loaded for this script.
        /// Use this before uploading a new script for this play.
        /// Sets character count to zero.
        /// Removes any texts related to this script.
        /// Deletes any Elements created for previous scripts
        /// </summary>
        public void CleanTextsAndElements()
        {
            using (CaptionDataContext data = new CaptionDataContext())
            {
                CleanTexts(data);
                CleanElements(data);
            }
        }

        // Calls database directly to delete all elements for this script
        public void CleanTexts(CaptionDataContext data)
        {
            data.Database.ExecuteSqlCommand(string.Format("DELETE FROM Texts WHERE ScriptId = {0}", Id));
        }

        // Calls database directly to delete all texts
        public void CleanElements(CaptionDataContext data)
        {
            data.Database.ExecuteSqlCommand(string.Format("DELETE FROM Elements WHERE ScriptId = {0}", Id));
        }


        public bool HasTexts()
        {
            return CountTexts() > 0 ? true : false;
        }

        public int CountTexts()
        {
            using (CaptionDataContext db = new CaptionDataContext())
            {
                return db.Text.Count(t => t.Script.Id == Id);
            } 
        }

        public Encoding GetScriptEncoding()
        {
            return GetScriptEncoding(FileName);
        }

        public void RegenerateScriptWithCharacterCount(int? newCount)
        {
            base.RegenerateScriptWithCharacterCount(this, newCount);
        }
    }
}