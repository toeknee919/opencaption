﻿using System.ComponentModel.DataAnnotations.Schema;
namespace OpenCaption.Models
{
    public class Venue
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Name { get; set; }

    }
}