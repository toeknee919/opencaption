﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OpenCaption.Models
{
    public class Text
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int Sequence { get; set; }
        [Column(TypeName = "ntext")]
        [DisplayName("Content Text")]
        public string ContentText { get; set; } 

        public bool Visible { get; set; }

        [DisplayName("Operator Note")]
        public string OperatorNote { get; set; }

        [DisplayName("Script ID")]
        public int ScriptId { get; set; }
        public virtual Script Script { get; set; }

        [DisplayName("Character Name")]
        public virtual Element Element { get; set; }
    }
}