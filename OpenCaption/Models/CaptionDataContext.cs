﻿using System.Data.Entity;

namespace OpenCaption.Models
{
    public class CaptionDataContext : DbContext
    {
        public DbSet<Work> Work { get; set; }

        public DbSet<Text> Text { get; set; }

        public DbSet<Element> Element { get; set; }
        
        public DbSet<Venue> Venue { get; set; }

        public DbSet<Operator> Operator { get; set; }

        public DbSet<Script> Script { get; set; }

        public CaptionDataContext()
        {
            Database.CreateIfNotExists();
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<CaptionDataContext>());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Properties()
                    .Where(p => p.Name == "Id")
                    .Configure(p => p.IsKey());
        }
    }
}