﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OpenCaption.Models
{
    public class Element
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [DisplayName("Display Name")]
        public string Name { get; set; }

        public int ScriptId { get; set; }
        public virtual Script Script { get; set; }

        public string Type { get; set; }

    }
}