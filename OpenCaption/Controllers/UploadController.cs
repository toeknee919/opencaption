﻿using System;
using System.Collections.Generic;
using OpenCaption.Models;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Threading.Tasks;

namespace OpenCaption.Controllers
{
    public class UploadController : Controller
    {
        // GET: Upload
        public ActionResult Index(bool? upload)
        {
            using (CaptionDataContext data = new CaptionDataContext())
            {
                ViewData["Work"] = data.Work.ToList();
                ViewData["Venue"] = new SelectList(data.Venue.ToList(), "Id", "Name");
                ViewData["CharacterCount"] = new SelectList(Enumerable.Range(20, 81));
                ViewData["Upload"] = upload == true ? "true" : "false";
                return View(data);
            }
        }

        /// <summary>
        /// Starts the upload of a script into the database in a new thread. 
        /// </summary>
        /// <param name="scriptFile">File uploaded in the form</param>
        /// <param name="Venue"></param>
        /// <param name="Work"></param>
        /// <param name="CharacterCount"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Index(HttpPostedFileBase scriptFile, int Venue, int Work, string Language, int CharacterCount = Int32.MaxValue)
        {
            // Save the file to the server
            if (scriptFile != null && scriptFile.ContentLength > 0)
            {
                // extract only the filename
                var fileName = Path.GetFileName(scriptFile.FileName);

                // store the file inside ~/App_Data/uploads folder
                var path = Path.Combine(Server.MapPath("~/App_Data/"), fileName);
                scriptFile.SaveAs(path);

                // Create the object we will be uploading
                Script script = new Script(){ FileName = path.ToString(), Original = true, VenueId = Venue, WorkId = Work, CharacterCount = CharacterCount, Language = Language } ;
                
                // Start the parsing process in the background
                Task.Factory.StartNew(() => StartParse(script));
            }

            // Send the user back to work while it is parsed
            return RedirectToAction("Index", new { upload = true });
        }

        /// <summary>
        /// Starts the parsing of a script. Notifies the user 
        /// via a SignalR alert when the upload has completed. 
        /// </summary>
        /// <param name="script"></param>
        private void StartParse(Script script)
        {
            var alert = new ScriptAlertHub();

            try
            {
                // Parse The script
                var watch = System.Diagnostics.Stopwatch.StartNew();
                script.ParseNewScript();
                watch.Stop();
                alert.AlertSuccess("Script " + Path.GetFileName(script.FileName) + " upload finished in " + watch.ElapsedMilliseconds.ToString() + "ms");
            }
            catch(Exception ex)
            {
                alert.AlertFail("Script " + Path.GetFileName(script.FileName) + " upload failed with message: " + ex.Message);
            }
        }
    } 
}