﻿using System.Linq;
using System.Web.Mvc;
using OpenCaption.Models;

namespace OpenCaption.Controllers
{
    public class HomeController : Controller
    {
        private CaptionDataContext db = new CaptionDataContext();

        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Asks the user to select a work to edit
        /// </summary>
        /// <returns></returns>
        public ActionResult SelectWork()
        {
            return View(db.Work.ToList());
        }
    }
}