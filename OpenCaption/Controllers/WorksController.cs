﻿using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using PagedList;
using System.Web.Mvc;
using OpenCaption.Models;

namespace OpenCaption.Views
{
    public class WorksController : Controller
    {
        private CaptionDataContext db = new CaptionDataContext();

        // GET: Works
        public ActionResult Index()
        {
            return View(db.Work.Include(w => w.Scripts).ToList());
        }

        // GET: Works/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Work work = db.Work.Find(id);
            if (work == null)
            {
                return HttpNotFound();
            }
            return View(work);
        }

        // GET: Works/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Works/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name")] Work work)
        {
            if (ModelState.IsValid)
            {
                db.Work.Add(work);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(work);
        }

        // GET: Works/Edit/5
        [Route("Works/{Id}/ViewScripts")]
        public ActionResult ViewScripts(int? Id)
        {
            if (Id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            List<Script> scripts = db.Script.Where(s => s.WorkId == Id).Include(s => s.Work).ToList();
            var title = scripts.FirstOrDefault();
            if(title != null)
            {
                ViewData["name"] = title.Work.Name;
            }
            return View(scripts);
        }

        // POST: Works/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,CharacterCount")] Work work)
        {
            if (ModelState.IsValid)
            {
                db.Entry(work).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(work);
        }

        // GET: Works/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Work work = db.Work.Find(id);
            if (work == null)
            {
                return HttpNotFound();
            }
            return View(work);
        }

        // POST: Works/Delete/5
        [HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Work work = db.Work.Find(id);
            db.Work.Remove(work);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult HasTexts(int id)
        {
            return View();
        }

        public ActionResult Export(int id)
        {
            ScriptManager.GenerateTextScript(id, HttpContext.Server.MapPath("~/App_Data/").ToString());
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
