﻿using OpenCaption.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Data.Entity;

namespace OpenCaption.Controllers
{
    public class AdminController : Controller
    {
        private CaptionDataContext db = new CaptionDataContext();

        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Export()
        {
            return View(db.Work.ToList());
        }

    }
}