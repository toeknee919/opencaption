﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Threading.Tasks;
using System;
using PagedList;

namespace OpenCaption.Models
{
    public class ScriptController : Controller
    {
        private CaptionDataContext db = new CaptionDataContext();

        // GET: Script
        public ActionResult Index()
        {
            return View(db.Script.ToList());
        }

        // GET: Script/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Script script = db.Script.Find(id);
            if (script == null)
            {
                return HttpNotFound();
            }
            return View(script);
        }

        // GET: Script/Create
        public ActionResult Create()
        {
            return View();
        }

        // GET: Works/Edit/5
        [Route("Script/{ScriptId}/EditText")]
        public ActionResult EditText(int? ScriptId, int? page)
        {
            int pageSize = 100;
            int pageNumber = (page ?? 1);
            if (ScriptId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            List<Text> text = db.Text.Where(t => t.ScriptId == ScriptId).Include(t => t.Script.Work).ToList();
            if (text == null)
            {
                return HttpNotFound();
            }
            return View(text.ToPagedList(pageNumber, pageSize));
        }

        // POST: Script/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,FileName,WorkId,VenueId,CharacterCount,Original,Language,CreatedDate")] Script script)
        {
            if (ModelState.IsValid)
            {
                db.Script.Add(script);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(script);
        }

        // GET: Script/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Script script = db.Script.Find(id);
            ViewData["NewCount"] = new SelectList(Enumerable.Range(25, 25), 33) as IEnumerable<SelectListItem>;
            if (script == null)
            {
                return HttpNotFound();
            }
            return View(script);
        }

        [Route("Script/{workId}/AdjustCharacterCount/{id}")]
        public ActionResult AdjustCharacterCount(int? id, int? workId)
        {
            if (id == null || workId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Script script = db.Script.Where(s => s.Id == id).Include(s => s.Work).First();
            ViewData["NewCount"] = new SelectList(Enumerable.Range(20, 81)) as IEnumerable<SelectListItem>;
            if (script == null)
            {
                return HttpNotFound();
            }
            return View(script);
        }

        // POST: Script/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,FileName,WorkId,VenueId,CharacterCount,Original,Language,CreatedDate")] Script script)
        {
            if (ModelState.IsValid)
            {
                db.Entry(script).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(script);
        }

        // GET: Script/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Script script = db.Script.Find(id);
            if (script == null)
            {
                return HttpNotFound();
            }
            return View(script);
        }

        // POST: Script/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Script script = db.Script.Find(id);
            db.Script.Remove(script);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // POST: Script/AdjustCount/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AdjustCount(int? id, int? newCount)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Script script = db.Script.Find(id);           
            Task.Factory.StartNew(() => AdjustCharacterCountForScript(script, newCount));
            return RedirectToAction("Index", "Admin");
        }

        [Route("Script/{workId}/ExportScript")]
        public ActionResult ExportScript(bool? upload, int workId)
        {
            ViewData["Upload"] = upload == true ? "true" : "false";
            List<Script> scripts = db.Script.Where(s => s.WorkId == workId).Include(s => s.Work).ToList();
            var title = scripts.FirstOrDefault();
            if (title != null)
            {
                ViewData["name"] = title.Work.Name;
            }
            return View(scripts);
        }

        [Route("Script/{workId}/ExportScript/{scriptId}")]
        public ActionResult ExportScript(int scriptId)
        {
            // Start the parsing process in the background
            Task.Factory.StartNew(() => StartExport(scriptId));

            return RedirectToAction("Index", new { upload = true });
        }

        /// <summary>
        /// Thread to export a script for the user to download and edit.
        /// </summary>
        /// <param name="scriptId"></param>
        private void StartExport(int scriptId)
        {
            var alert = new ScriptAlertHub();
            try
            {
                var watch = System.Diagnostics.Stopwatch.StartNew();
                ScriptManager.GenerateTextScript(scriptId, HttpContext.Server.MapPath("~/App_Data/").ToString());
                watch.Stop();
                alert.AlertSuccess("Export of script " + scriptId + " finished in " + watch.ElapsedMilliseconds.ToString() + "ms");
            }
            catch (Exception ex)
            {
                alert.AlertFail("Export to file failed with message" + ex.Message);
            }

        }

        private void AdjustCharacterCountForScript(Script script, int? newCount)
        {
            var alert = new ScriptAlertHub();
            try
            {
                var watch = System.Diagnostics.Stopwatch.StartNew();
                script.RegenerateScriptWithCharacterCount(newCount);
                watch.Stop();
                alert.AlertSuccess("Export of script " + script.Id + " finished in " + watch.ElapsedMilliseconds.ToString() + "ms");
            }
            catch(Exception ex)
            {
                alert.AlertFail("Adjust Character Count failed with message" + ex.Message);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
