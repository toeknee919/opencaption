<?php session_start(); 
// Author: 	Tony Hess
// File: 			metardecoder
// Purpose: 	This file is main webpage used to display the user, their name, their home weather report
//					their primary aircraft type, their favorite airports weather, and the users certificates.

    $dbHost = "oniddb.cws.oregonstate.edu";   //Location Of Database/localhost 
    $dbUser = "hessan-db";           		 //Database User Name 
    $dbPass = "9BfMc4EtrI4GtOw7";            //Database Password 
    $dbDatabase = "hessan-db";    			//Database Name 
     
    $db = mysql_connect($dbHost,$dbUser,$dbPass)or die("Error connecting to database."); 
    //Connect to the database 
    mysql_select_db($dbDatabase, $db)or die("Couldn't select the database."); 



//redirect if needed
if(!isset($_SESSION['username'])){
header("Location: weather.php"); 
}

include ("phpmyeasyweather.inc.php");
$homeID = strtoupper($_SESSION['homeweax']); 
$usr = $_SESSION['username'];
$id = $_SESSION['id'];
?>


<!DOCTYPE html>
<head>
<meta charset='utf-8'>
<title>Personal Weather Data</title>
		<style>	
			body{
				min-width: 1000px;
				max-width: 1000px;
			}
			h1{
				font-family:Sans-serif;
				text-align: center;
				color:#ffffff;
				background-color:#898989;
				border-style: ridge;
				border-width: 12px;
			}
			#homeWx{
				border:5px outset;
				background-color: #a5d8ff;
			}
			#certs, #airports{
				border:4px outset;
				background-color: #a5d8ff;
				float: right;
			}
			#airports{
				border:4px outset;
				background-color: #a5d8ff;
				float: left;
				max-width: 695px;
			}
			#homeForm{
				float: right;
			}
			input[type=text]{
				float: right;
				width: 50px;
				height: 16px;
			}
			input[type=submit]{
				float: right;
				width: 100px;
				height: 22px;
			}
			#exp, #ratings{
				float: right;
				text-align: right;
			}
		</style>
		
		
<?php 
//gets the metar for the home weather report
if ($input = getMetarFromWWW ($homeID)) {
   $METAR = $input["metar"];
} 
else {$METAR = "No METAR found!";}
?>


<script src="jquery-1.11.0.min.js"></script>
<script type="text/javascript">

window.onload = function(){

document.getElementById("myMetar").innerHTML = "<?php echo $METAR . "<br>";?>";

document.getElementById("myTaf").innerHTML = "<?php 

									if ($inpu = getTafFromWWW($homeID)) {
									   $TAF  = $inpu["taf"];

										$tafArr = getTafParts($TAF);
										for ($bar=0;$bar<count($tafArr);$bar++) {
											echo $tafArr[$bar] . "<br>";
										} 
									}
									else{
									   echo "no TAF information found!";
									}; ?>";		

				
}


</script>	

</head>


<body>

<h1>Welcome to your WEAX Network.</h1>
<b>User:</b> <?php echo $_SESSION['username'];?><br>
<b>Primary Aircraft:</b> <?php echo $_SESSION['p_acft'];?>



   <!-- Button for updating the users home weather -->
<div id="homeWx">
<form method="post" action="homeUpdate.php" name="update">
<input type="Submit" name="update" value="Change Home">
<input type="text" placeholder="KPDX" maxlength="4" name="update">		
</form>

   <!-- Holds the home base weather report -->
<b>Here is your current home weather at <?php echo $homeID; ?></b>

<div id="myMetar">Retrieving METAR</div>
<div id="exp">Changes your home <br>weather reporting station</div>
<b>Forecast</b>
<div id="myTaf">Retrieving TAF</div>	

</div>


 <!-- Form for updating/displaying the pilots ratings -->
<div id = "certs">

 <!-- Displays the pilots current ratings -->
	<div id = "ratings"> 
		<b>HERE ARE YOUR CURRENT RATINGS</b><br> 
			<?php 
				$sql = mysql_query("SELECT * FROM weatherUser INNER JOIN
									Pilots_Cert ON weatherUser.id = Pilots_Cert.id INNER JOIN
									Certificates ON Pilots_Cert.cid = Certificates.cid
									WHERE username = '$usr'");
				while ($row = mysql_fetch_array($sql)){
				echo $row['title'] . "</br>";
				}
			?>
	</div>
	
	<form method="post" action="homeUpdate.php" name="update_cert">
		<input type="Submit" name="update_cert" value="Add Cert">
		<select name = "cert">
			<?php 
				$sql = mysql_query("SELECT * FROM Certificates");
				while ($row = mysql_fetch_array($sql)){
				echo "<option>" . $row['title'] . "</option>";
				}
			?>
		</select><br>	
	</form>
</div>	
	


	<!-- Displays the users favorite airports  and form for adding additional options -->

	<div id = "airports"> 
		<b>HERE ARE YOUR FAVORITE LOCATIONS</b><br> 
			<?php 
				$sql = mysql_query("SELECT * FROM fav_apts
									WHERE id = '$id'");
				while ($row = mysql_fetch_array($sql)){
					if ($input = getMetarFromWWW ($row['apt_ident'])) {
						$MET = $input["metar"];
						} 
						else {$MET = "No METAR found!";}				
				echo "<b>" . $row['apt_ident'] . "</b></br>" . $MET . "<br><br>";
				}
			?>

	
		<form method="post" action="homeUpdate.php" name="add_fav">
				<input type="Submit" name="add_fav" value="Add Airport.">
				<input type="text" placeholder="KPDX" maxlength="4" name="new_apt">		
		</form>
	</div>

	
</body>
