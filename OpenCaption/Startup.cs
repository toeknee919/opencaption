﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(OpenCaption.Startup))]
namespace OpenCaption
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            //ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}
